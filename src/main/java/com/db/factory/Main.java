package com.db.factory;

/**
 * Created by dpr on 13/04/2015.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        ObjectFactory.getInstance().createObject(RobotImpl.class).cleanRoom();
    }
}
