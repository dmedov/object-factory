package com.db.factory;

/**
 * Created by dpr on 14/04/2015.
 */
public interface Config {
    <T> Class<? extends T> getImpl(Class<T> ifc);
}
