package com.db.factory;

/**
 * Created by dpr on 15/04/2015.
 */
public interface ProxyConfigurer {
    <T> T getProxy(T t, Class type);
}
