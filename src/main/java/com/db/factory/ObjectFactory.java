package com.db.factory;

import org.reflections.Reflections;

import javax.annotation.PostConstruct;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by dpr on 14/04/2015.
 */
public class ObjectFactory {
    private static ObjectFactory ourInstance = new ObjectFactory();

    public static ObjectFactory getInstance() {
        return ourInstance;
    }

    private Config config = new JavaConfig();
    private Reflections reflections = new Reflections("com.db.factory");
    private List<ObjectConfigurer> objectConfigurers = new ArrayList<>();
    private List<BaseProxyConfigurer> proxyConfigurers = new ArrayList<>();

    private ObjectFactory() {
        Set<Class<? extends BaseProxyConfigurer>> proxys = reflections.getSubTypesOf(BaseProxyConfigurer.class);

        try {
            for (Class<? extends BaseProxyConfigurer> proxy : proxys) {
                proxyConfigurers.add(proxy.newInstance());
            }

            Set<Class<? extends ObjectConfigurer>> classes = reflections.getSubTypesOf(ObjectConfigurer.class);
            for (Class<? extends ObjectConfigurer> configurerClass : classes) {
                    objectConfigurers.add(configurerClass.newInstance());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public <T> T createObject(Class<T> type) throws Exception {
        if (type.isInterface()) {
            type = findImpl(type);
        }

        T t = type.newInstance();
        configure(t);

        invokeInitMethods(type, t);

        t = getProxys(t, type);

        return t;
    }

    private <T> T getProxys(T t, Class type) {
        for (BaseProxyConfigurer proxyConfigurer : proxyConfigurers) {
            t = proxyConfigurer.getProxy(t, type);
        }

        return t;
    }

    private <T> void invokeInitMethods(Class<T> type, T t) throws IllegalAccessException, InvocationTargetException {
        Method[] methods = type.getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(PostConstruct.class)) {
                method.invoke(t);
            }
        }
    }

    private <T> void configure(T t) throws Exception {
        for (ObjectConfigurer configurer : objectConfigurers) {
            configurer.configure(t);
        }
    }

    private <T> Class<T> findImpl(Class<T> type) {
        Class<? extends T> impl = config.getImpl(type);
        if (impl == null) {
            Set<Class<? extends T>> classes = reflections.getSubTypesOf(type);
            if (classes.size() != 1) {
                throw new RuntimeException("0 or more than one impl found to interface " + type + " define the wanted impl in config");
            }
            type= (Class<T>) classes.iterator().next();
        }else {
            type = (Class<T>) impl;
        }
        return type;
    }


}
