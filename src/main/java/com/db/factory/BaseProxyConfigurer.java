package com.db.factory;

import net.sf.cglib.proxy.Enhancer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by dpr on 15/04/15.
 */
public abstract class BaseProxyConfigurer implements ProxyConfigurer {

    public abstract <T> T getProxy(T t, Class type);
    public abstract Object handle(Class type, Object t, Object o, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException;

    public <T> T getInterfaceProxy(final T t, final Class type) {
        return (T) Proxy.newProxyInstance(type.getClassLoader(), type.getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return handle(type, t, proxy, method, args);
            }
        });
    }

    public <T> T getSubClass(final T t, final Class type) {
        return (T) Enhancer.create(type, new net.sf.cglib.proxy.InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] args) throws Throwable {
                return handle(type, t, o, method, args);
            }
        });
    }
}
