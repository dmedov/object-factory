package com.db.factory;

/**
 * Created by dpr on 14/04/2015.
 */
public interface ObjectConfigurer {
    void configure(Object o) throws Exception;
}
