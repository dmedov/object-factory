package com.db.factory;

import javax.annotation.PostConstruct;

/**
 * Created by dpr on 13/04/2015.
 */
@Benchmark
public class RobotImpl implements Robot {

    @PostConstruct
    public void init() {
        System.out.println(cleaner.getClass());
    }

    @Inject
    private Cleaner cleaner;

    @Inject
    private Speaker speaker;

    @Override
    @Transactional
    @Benchmark
    public void cleanRoom() {
        speaker.speak("I started my work");
        cleaner.clean();
        speaker.speak("I finished my work");
    }
}
