package com.db.factory;

import java.lang.reflect.Field;
import java.util.Random;

/**
 * Created by dpr on 14/04/2015.
 */
public class InjectRandomIntObjectConfigurer implements ObjectConfigurer {
    @Override
    public void configure(Object t) throws IllegalAccessException {
        Field[] fields = t.getClass().getDeclaredFields();
        Random random = new Random();
        for (Field field : fields) {
            InjectRandomInt annotation = field.getAnnotation(InjectRandomInt.class);
            if (annotation != null) {
                field.setAccessible(true);
                int min = annotation.min();
                int max = annotation.max();
                field.set(t,min+random.nextInt(max-min)+1);
            }
        }
    }
}
