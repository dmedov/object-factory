package com.db.factory;

/**
 * Created by dpr on 13/04/2015.
 */
public interface Cleaner {
    void clean();
}
