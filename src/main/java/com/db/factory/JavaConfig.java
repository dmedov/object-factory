package com.db.factory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dpr on 14/04/2015.
 */
public class JavaConfig implements Config {

    private Map<Class, Class> ifc2Class = new HashMap<>();

    public JavaConfig() {
        ifc2Class.put(Speaker.class, ConsoleSpeaker.class);
        ifc2Class.put(Cleaner.class, CleanerImpl.class);
    }

    @Override
    public <T> Class<? extends T> getImpl(Class<T> ifc) {
        return ifc2Class.get(ifc);
    }
}
