package com.db.factory;

import java.lang.reflect.Field;

/**
 * Created by dpr on 14/04/2015.
 */
public class InjectAnnotationObjectConfigurer implements ObjectConfigurer {
    @Override
    public void configure(Object t) throws Exception {
        Field[] fields = t.getClass().getDeclaredFields();
        for (Field field : fields) {
            Inject annotation = field.getAnnotation(Inject.class);
            if (annotation != null) {
                field.setAccessible(true);
                field.set(t,ObjectFactory.getInstance().createObject(field.getType()));
            }
        }
    }
}
