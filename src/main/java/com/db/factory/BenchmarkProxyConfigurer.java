package com.db.factory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by dpr on 15/04/15.
 */
public class BenchmarkProxyConfigurer extends BaseProxyConfigurer {

    public <T> T getProxy(T t, Class type) {
        if (type.isAnnotationPresent(Benchmark.class)) {
            if (type.getInterfaces().length == 0) {
                t = getSubClass(t, type);
            } else {
                t =  getInterfaceProxy(t, type);
            }
        }
        return t;
    }

    @Override
    public Object handle(Class type, Object t, Object o, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Method originalMethod = type.getMethod(method.getName(), method.getParameterTypes());

        if (originalMethod.isAnnotationPresent(Benchmark.class)) {
            System.out.println("*************BENCHMARK START*****************");
            long before = System.nanoTime();
            Object retVal = method.invoke(t, args);
            long after = System.nanoTime();
            System.out.println(method.getName() + " time: " + (after - before));
            System.out.println("*************BENCHMARK END*****************");
            return retVal;
        }

        return method.invoke(t, args);
    }
}
