package com.db.factory;

import javax.annotation.PostConstruct;

/**
 * Created by dpr on 13/04/2015.
 */
@Benchmark
public class CleanerImpl implements Cleaner {

    @InjectRandomInt(min = 3, max = 7)
    private int repeat;
    private int vvv=666;

    @PostConstruct
    public void init(){
        System.out.println(vvv);
    }

    @Override
    public void clean() {
        for (int i = 0; i < repeat; i++) {
            System.out.println("VVVVVVVVvvvvvvvvvvvvvvvvvv");
        }
    }
}
