package com.db.factory;

/**
 * Created by dpr on 13/04/2015.
 */
public class ConsoleSpeaker implements Speaker {
    @Override
    public void speak(String message) {
        System.out.println(message);
    }
}
