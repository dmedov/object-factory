package com.db.factory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by dpr on 15/04/15.
 */
public class TransactionProxyConfigurer extends BaseProxyConfigurer {
    public <T> T getProxy(T t, Class type) {
        if (type.getInterfaces().length == 0) {
            t = getSubClass(t, type);
        } else {
            t =  getInterfaceProxy(t, type);
        }
        return t;
    }

    @Override
    public Object handle(Class type, Object t, Object o, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Method originalMethod = type.getMethod(method.getName(), method.getParameterTypes());

        if (originalMethod.isAnnotationPresent(Transactional.class)) {
            System.out.println("*************TRANSACTION BEGIN*****************");
            Object retVal = method.invoke(t, args);
            System.out.println("*************TRANSACTION END*****************");
            return retVal;
        }

        return method.invoke(t, args);
    }
}
