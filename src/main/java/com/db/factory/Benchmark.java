package com.db.factory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by dpr on 15/04/15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Benchmark {
}
