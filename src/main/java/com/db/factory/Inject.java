package com.db.factory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by dpr on 13/04/2015.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
}
